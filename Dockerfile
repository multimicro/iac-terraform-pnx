# Create vulnerabilities based on kics queries in https://docs.kics.io/latest/queries/dockerfile-queries/ 
FROM debian:latest 

# kics: Run Using Sudo
# kics: Run Using apt 
RUN sudo apt install git 

# kics: UNIX Ports Out Of Range
EXPOSE 99999

# kics: Multiple ENTRYPOINT Instructions Listed
ENTRYPOINT ["ex1"]
ENTRYPOINT ["ex2"]
ENTRYPOINT ["ex3"]
