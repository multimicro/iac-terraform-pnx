# Infrastructure As Code Scanning

Demo project for [Infrastructure as Code Scanning](https://docs.gitlab.com/ee/user/application_security/iac_scanning/) introduced in [GitLab 14.5](https://about.gitlab.com/releases/2021/11/22/gitlab-14-5-released/#introducing-infrastructure-as-code-iac-security-scanning).

- [Dockerfile](Dockerfile) uses bad practices [detected by kics queries](https://docs.kics.io/latest/queries/dockerfile-queries/)
- [terraform/](terraform/) creates an AWS S3 bucket which is world readable and [detected by queries](https://docs.kics.io/latest/queries/terraform-queries/)

## Instructions

- Fork/copy the project
  - CI/CD > Pipelines > Run manually
- Inspect the security reports
  - As MR widget: https://gitlab.com/gitlab-de/playground/infrastructure-as-code-scanning/-/merge_requests/1 
  - Navigate to Security & Compliance > Vulnerability Report


## Screenshots

![Vulnerability reports](docs/images/iac_scans_vuln_reports.png)
![Detail for Terraform AWS S3](docs/images/iac_scans_vuln_details_terraform_aws_s3.png)
![Code highlight for Terraform AWS S3](docs/images/iac_scans_vuln_details_terraform_aws_s3_code.png)
![Code highlight for Docker](docs/images/iac_scans_vuln_details_docker_code.png)

